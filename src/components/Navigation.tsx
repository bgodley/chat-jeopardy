import { signIn, signOut, useSession } from "next-auth/react";
import Link from "next/link";

const Navigation: React.FC = () => {
  const { data: session } = useSession();
  return (
    <div className="navbar sticky top-0 z-10 bg-base-200">
      <div className="navbar-start">
        <div className="dropdown">
          <label tabIndex={0} className="btn-ghost btn lg:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h8m-8 6h16"
              />
            </svg>
          </label>
          <ul
            tabIndex={0}
            className="dropdown-content menu rounded-box menu-sm mt-3 w-52 bg-base-100 p-2 shadow"
          >
            <li>
              <Link className="btn-primary btn-sm btn" href="/play">
                Play
              </Link>
            </li>
            {!!session && <li>
              <Link className="btn-ghost btn-sm btn" href="/account">
                My Profile
              </Link>
            </li>}
            <li>
              <Link className="btn-ghost btn-sm btn" href="/leaderboard">
                Leaderboard
              </Link>
            </li>
          </ul>
        </div>
        <Link href="/" className="btn-ghost btn text-xl normal-case">
          ChatJeopardy
        </Link>
      </div>
      <div className="navbar-center hidden lg:flex">
        <ul className="flex gap-6">
          <li>
            <Link className="btn-primary btn-sm btn" href="/play">
              Play
            </Link>
          </li>
          {!!session && <li>
            <Link className="btn-ghost btn-sm btn" href="/account">
              My Profile
            </Link>
          </li>}
          {/* <li tabIndex={0}>
            <details>
              <summary>Parent</summary>
              <ul className="p-2">
                <li>
                  <a>Submenu 1</a>
                </li>
                <li>
                  <a>Submenu 2</a>
                </li>
              </ul>
            </details>
          </li> */}
          <li>
            <Link className="btn-ghost btn-sm btn" href="/leaderboard">
              Leaderboard
            </Link>
          </li>
        </ul>
      </div>
      <div className="navbar-end">
        {!!session ? <button className="btn-ghost btn-sm btn" onClick={() => void signOut()}>
          Sign out
        </button> : <>
          {/* <button className="btn-ghost btn-sm btn mr-6" onClick={() => void signIn()}>
            Sign in
          </button> */}
          <button className="btn-primary btn-sm btn" onClick={() => void signIn("auth0")}>
            Sign in
          </button>
        </>}
      </div>
    </div>
    // <nav className="sticky top-0 z-10 mb-10 shadow bg-base-200">
    //   <div className="items-centerv container mx-auto flex h-14 justify-between">
    //     <ul className="flex items-center gap-10">
    //       <li>
    //         <Link
    //           href="/"
    //           className="btn btn-primary"
    //         >
    //           Play
    //         </Link>
    //       </li>
    //       <li>
    //         <Link href="/account">
    //           My Profile
    //         </Link>
    //       </li>
    //       <li>
    //         <Link href="/leaderboard">
    //           Leaderboard
    //         </Link>
    //       </li>
    //     </ul>

    //     <ul className="flex items-center gap-10">
    //       <li
    //         onClick={() => void signOut()}
    //         className="cursor-pointer"
    //       >
    //         Log out
    //       </li>
    //     </ul>
    //   </div>
    // </nav>
  );
};

export default Navigation;
