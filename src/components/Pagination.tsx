import { createContext, useContext, useEffect } from "react";

interface PageContextData {
  page: number;
  numPages: number;
  setPage: (page: number) => void;
}

export const PageContext = createContext<PageContextData>({
  page: 1,
  numPages: 1,
  setPage: () => null,
});

const Pagination = () => {
  const { page, numPages, setPage } = useContext(PageContext);

  useEffect(() => {
    console.log(page, numPages, setPage);
  }, [page, numPages, setPage]);

  const pageSet = new Set<number>();

  if (numPages <= 6) {
    for (let i = 1; i <= numPages; i++) {
      pageSet.add(i);
    }
  } else {
    pageSet.add(1);
    pageSet.add(2);
    pageSet.add(page - 1);
    pageSet.add(page);
    pageSet.add(page + 1);
    pageSet.add(numPages - 1);
    pageSet.add(numPages);
  }

  const pageListWithElipses: Array<number | string> = [];

  const pageList = Array.from(pageSet)
    .filter((i) => i > 0 && i <= numPages)
    .sort((a, b) => a - b);

  pageList.forEach((i, k) => {
    const prev = pageList[k - 1];
    if (prev && i - prev > 1) {
      pageListWithElipses.push("...");
    }
    pageListWithElipses.push(i);
  });

  return (
    <div className="flex items-center gap-4">
      Page
      <div className="join shadow">
        <button
          className="btn-sm join-item btn"
          disabled={page <= 1}
          onClick={() => {
            if (page > 1) setPage(page - 1);
          }}
        >
          &lt;
        </button>
        {pageListWithElipses.map((i) => {
          if (typeof i === "string") {
            return (
              <span
                key={i}
                className="join-item flex w-10 items-center justify-center"
              >
                ...
              </span>
            );
          } else {
            return (
              <button
                key={i}
                className={
                  page === i
                    ? "btn-sm btn-active join-item btn"
                    : "btn-sm join-item btn"
                }
                onClick={() => {
                  setPage(i);
                }}
              >
                {i}
              </button>
            );
          }
        })}

        <button
          className="btn-sm join-item btn"
          disabled={page >= numPages}
          onClick={() => {
            if (page < numPages) setPage(page + 1);
          }}
        >
          &gt;
        </button>
      </div>
    </div>
  );
};

export default Pagination;
