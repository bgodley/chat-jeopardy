import { QuestionStatus, type Question } from "@prisma/client";
import { useSession } from "next-auth/react";

interface QuestionHistoryProps {
  question: Question;
}

const QuestionHistory: React.FC<QuestionHistoryProps> = ({ question }) => {
  const { data: session } = useSession();
  const refer = session?.user?.id === question.userId ? "You " : "They ";

  return <div className="rounded-md bg-base-200 p-4 shadow">
    <div className="flex gap-12">
      <div className="flex-1">
        <p className="text-sm text-gray-400">Category</p>
        <p className="text-xl">{question.category}</p>
        <p className="mt-2 text-sm text-gray-400">
          {new Date(question.createdAt).toLocaleDateString()}
          {" at "}
          {new Date(question.createdAt).toLocaleTimeString()}
        </p>
        {question.reconsidered && (
          <p className="mt-2 text-sm text-gray-400">
            Question was reconsidered
          </p>
        )}
      </div>
      <div className="flex-[4]">
        <p className="text-sm text-gray-400">Question</p>
        <p className="text-xl">{question.question}</p>

        {question.questionStatus === QuestionStatus.answered && (
          <>
            <p className="mt-4 text-sm text-gray-400">{refer} answered...</p>
            <p className="text-xl italic">
              &quot;{question.userAnswer ?? ""}&quot;
            </p>

            <p className="mt-4 text-sm text-gray-400">Correct answer</p>
            <p className="text-xl">{question.correctAnswer}</p>

            <p className="mt-4 text-sm text-gray-400">{refer} were...</p>
            <p
              className={
                question.wasCorrect
                  ? "text-2xl font-bold text-green-600 dark:text-green-500"
                  : "text-xl font-bold text-red-600 dark:text-red-500"
              }
            >
              {question.wasCorrect ? "Correct" : "Incorrect"}
            </p>
          </>
        )}
        {question.questionStatus === QuestionStatus.skipped && (
          <>
            <p className="mt-4 text-sm text-gray-400">{refer} answered...</p>
            <p className="text-xl">Skipped question</p>

            <p className="mt-4 text-sm text-gray-400">Correct answer</p>
            <p className="text-xl">{question.correctAnswer}</p>
          </>
        )}
      </div>
    </div>
  </div>;
};

export default QuestionHistory;
