import { Head, Html, Main, NextScript } from "next/document";

const Document: React.FC = () => {
  return (
    <Html lang="en" data-theme="winter">
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
};

export default Document;
