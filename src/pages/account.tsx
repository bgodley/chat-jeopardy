import Navigation from "@/components/Navigation";
import Pagination, { PageContext } from "@/components/Pagination";
import QuestionHistory from "@/components/QuestionHistory";
import { getServerAuthSession } from "@/server/auth";
import { api } from "@/utils/api";
import { type GetServerSidePropsContext, type NextPage } from "next";
import { signIn, useSession } from "next-auth/react";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";

const Account: NextPage = () => {
  const router = useRouter();
  const session = useSession();
  const [page, setPage] = useState<number>(1);
  const userId = (router.query.userId as string) ?? session.data?.user.id;

  if (!userId) {
    return (
      <div className="container mx-auto flex flex-col gap-[12px] px-2 pt-10">
        <h1 className="text-4xl font-bold">ChatJeopardy</h1>
        <p className="text-lg">You must be signed in to view your profile.</p>
        <div>
          <button
            className="btn-primary btn"
            onClick={() => void signIn("auth0")}
          >
            Sign in
          </button>
        </div>
      </div>
    );
  }

  const {
    data: questionHistory,
    isLoading: historyLoading,
    isError: historyError,
  } = api.account.getHistory.useQuery({
    page,
    userId,
  });

  const {
    data: questionStats,
    isLoading: statsLoading,
    isError: statsError,
  } = api.account.getStats.useQuery({ userId });

  const {
    data: accountData,
    isLoading: accountLoading,
    isError: accountError,
  } = api.account.getAccount.useQuery({ userId });
  return (
    <>
      <Head>
        <title>Account | ChatJeopardy</title>
        <meta
          name="description"
          content="Choose your category and answer trivia questions!"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navigation />
      <main className="container mx-auto mt-4 p-2">
        <div className="grid gap-10 xl:grid-cols-3">
          <div className="col-span-1">
            <div className="mb-4 flex flex-row gap-4">
              <div className="indicator">
                <div className="avatar">
                  <div className="mask mask-hexagon w-24">
                    <Image
                      src={accountData?.user.image ?? ""}
                      alt={accountData?.user.name ?? ""}
                      width={96}
                      height={96}
                      className="h-24 w-24"
                    />
                  </div>
                </div>
                {accountLoading && (
                  <div className="indicator-center badge badge-secondary indicator-middle indicator-item">
                    <span className="loading loading-dots"></span>
                  </div>
                )}
                {accountError && (
                  <div className="indicator-center badge badge-secondary indicator-middle indicator-item">
                    Error
                  </div>
                )}
              </div>
              <div>
                <p className="mb-2 text-4xl font-bold">
                  {!!accountData && accountData.user.name}
                </p>
              </div>
            </div>
            {statsLoading && (
              <p className="text-lg">
                Loading player stats{" "}
                <span className="loading loading-dots loading-md" />
              </p>
            )}
            {statsError && (
              <p className="text-lg text-red-500">Error loading player stats</p>
            )}
            {questionStats && (
              <>
                <p className="mb-1 text-sm text-gray-400">Question Stats</p>

                <div className=" flex w-full flex-wrap rounded-lg bg-base-200 shadow">
                  <div className="p-6">
                    <div className="stat-figure text-primary"></div>
                    <div className="stat-title">Correct</div>
                    <div className="stat-value text-primary">
                      {questionStats?.correctAnswers}
                    </div>
                    <div className="stat-desc">{`${Math.ceil(
                      (questionStats?.correctAnswers /
                        questionStats?.answeredQuestions) *
                        100
                    )}% accuracy`}</div>
                  </div>

                  <div className="p-6">
                    <div className="stat-figure text-primary"></div>
                    <div className="stat-title">Answered</div>
                    <div className="stat-value text-primary">
                      {questionStats?.answeredQuestions}
                    </div>
                    <div className="stat-desc">
                      Answered questions <br />
                      count towards accuracy
                    </div>
                  </div>

                  <div className="p-6">
                    <div className="stat-figure text-primary"></div>
                    <div className="stat-title">Skipped</div>
                    <div className="stat-value text-primary">
                      {questionStats?.skippedQuestions}
                    </div>
                    <div className="stat-desc">
                      Skipped questions don&apos;t
                      <br />
                      count against accuracy
                    </div>
                  </div>
                </div>
                <div className="mt-4">
                  <p className="text-sm text-gray-400">Top Categories</p>
                  <div className="mt-2 flex flex-row flex-wrap items-center gap-4">
                    {questionStats?.topCategories.map((category, i) => (
                      <div
                        className="rounded bg-primary px-4 py-2 text-primary-content"
                        key={i}
                      >
                        {category.category} ({category.count})
                      </div>
                    ))}
                  </div>
                </div>
              </>
            )}
          </div>
          <div className="col-span-2">
            <h2 className="mb-4 text-4xl font-bold">Question History</h2>
            <div className="flex flex-col gap-6">
              {historyLoading && (
                <span className="loading loading-dots loading-md" />
              )}
              {historyError && (
                <span className="text-red-500">Error loading history</span>
              )}
              {questionHistory && (
                <PageContext.Provider
                  value={{
                    numPages: Math.ceil(questionHistory.total / 10),
                    page,
                    setPage,
                  }}
                >
                  <Pagination />
                  {questionHistory.questions.map((question, i) => (
                    <QuestionHistory key={i} question={question} />
                  ))}
                  <Pagination />
                </PageContext.Provider>
              )}
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Account;

export async function getServerSideProps(context: GetServerSidePropsContext) {
  return {
    props: {
      session: await getServerAuthSession(context),
    },
  };
}
