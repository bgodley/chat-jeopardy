import { env } from "@/env.mjs";
import { prisma } from "@/server/db";
import { chat } from "@/utils/chat";
import type { NextApiRequest, NextApiResponse } from "next";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { query } = req;

  if (query.secret !== env.CRON_SECRET) {
    return res.status(404).end();
  }
  console.log("Generating questions");
  const startTime = Date.now();

  const {
    response: { content: category },
  } = await chat([
    {
      content:
        "Please generate a trivia category that is unique. Respond only with the name of the category",
      role: "system",
    },
  ]);

  const {
    response: { content: questionsRaw },
  } = await chat([
    {
      role: "system",
      content: `Please generate 10 questions for the ${category} category. Format the questions as follows:
      1. Q: What is the capital of the United States? A: Washington, D.C.`,
    },
  ]);

  type Question = {
    question: string;
    correctAnswer: string;
  };

  const questions = questionsRaw
    .split("\n")
    .map((line) => {
      const regex = /\d{1,2}\. Q:\s*(.+)\s*A:(.*)/g;

      const match = regex.exec(line);
      if (!match) {
        return null;
      }
      const question = match[1]?.trim();
      const correctAnswer = match[2]?.trim();
      if (!question || !correctAnswer) {
        return null;
      }

      return {
        question,
        correctAnswer,
      };
    })
    .filter((question) => question !== null) as Question[];

  type RatedQuestion = Question & { rating: number };

  const ratingPromises: Array<Promise<RatedQuestion | null>> = [];

  for (const question of questions) {
    const ratingPromise = new Promise<RatedQuestion | null>(
      (resolve, _reject) => {
        chat([
          {
            role: "system",
            content: `Please rate the following question and answer pair on a scale of 1-10.
              Category: ${category}
              Question: ${question.question}
              Correct Answer: ${question.correctAnswer}
              
              Please evaluate the question and answer pair based on the following criteria:
              1. Is the question and answer pair grammatically correct?
              2. Is the question and answer pair factually correct?
              3. Is the question and answer pair relevant to the category?
              
              Please respond with a single number between 1 and 10, where 1 is the worst and 10 is the best.`,
          },
        ])
          .then(({ response: { content: ratingRaw } }) => {
            const ratingString = /(\d{1,2})/g.exec(ratingRaw)?.[1];
            if (ratingString === undefined) {
              resolve(null);
              return;
            }

            const rating = parseInt(ratingString);
            if (rating < 7) {
              resolve(null);
              return;
            }

            const rated: RatedQuestion = {
              ...question,
              rating,
            };
            resolve(rated);
          })
          .catch((err) => {
            console.error(err);
            resolve(null);
          });
      }
    );
    ratingPromises.push(ratingPromise);
  }

  const questionsRated = (await Promise.all(ratingPromises)).filter(
    (question) => question !== null
  ) as RatedQuestion[];

  const endTime = Date.now();
  const seconds = ((endTime - startTime) / 1000).toFixed(2);
  console.log(`Generated questions in ${seconds}s`);

  const result = await prisma.generatedQuestion.createMany({
    data: questionsRated.map((question) => ({
      question: question.question,
      correctAnswer: question.correctAnswer,
      category,
      autoRating: question.rating,
    })),
  });

  console.log(`Added ${result.count} questions to the database`);

  res.status(200).json({
    seconds,
    added: result.count,
    category,
    questions: questionsRated,
  });
};

export default handler;
