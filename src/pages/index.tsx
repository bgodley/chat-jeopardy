import Navigation from "@/components/Navigation";
import { type NextPage } from "next";
import { signIn } from "next-auth/react";
import Head from "next/head";

const Leaderboard: NextPage = () => {
  return (
    <>
      <Head>
        <title>ChatJeopardy</title>
        <meta name="description" content="Choose your category and answer trivia questions!" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navigation />
      <main>
        <div
          className="hero"
        >
          <div className="hero-overlay bg-opacity-60"></div>
          <div className="hero-content text-center text-neutral-content">
            <div className="max-w-md">
              <h1 className="mb-5 text-5xl font-bold">ChatJeopardy</h1>
              <p className="mb-5">
                A free-form trivia game powered by AI. Choose any category and answer questions in your own words.
              </p>
              <button className="btn-primary btn" onClick={() => void signIn("auth0")}>Sign up to Play</button>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Leaderboard;
