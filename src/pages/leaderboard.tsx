import Navigation from "@/components/Navigation";
import Pagination, { PageContext } from "@/components/Pagination";
import { api } from "@/utils/api";
import { ArrowDownIcon, ArrowUpIcon } from "@heroicons/react/24/solid";
import { type NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const Leaderboard: NextPage = () => {
  const [page, setPage] = useState<number>(1);
  const router = useRouter();

  const [sort, setSort] = useState<
    "questionsAnswered" | "questionsCorrect" | "accuracy" | undefined
  >();
  const [order, setOrder] = useState<"asc" | "desc" | undefined>();

  useEffect(() => {
    if (!router.isReady) return;

    let initialSort = router.query.sort;
    if (
      initialSort !== "questionsAnswered" &&
      initialSort !== "questionsCorrect" &&
      initialSort !== "accuracy"
    )
      initialSort = undefined;

    let initialOrder = router.query.order;
    if (initialOrder !== "desc" && initialOrder !== "asc")
      initialOrder = undefined;
    setSort(initialSort ?? "questionsAnswered");
    setOrder(initialOrder ?? "desc");
  }, [router.isReady, router.query.sort, router.query.order]);

  const { data: leaderboardData } = api.leaderboard.getLeaderboard.useQuery(
    {
      page: 1,
      sort,
      order,
    },
    {
      enabled: !!sort && !!order,
      staleTime: 1000 * 60 * 5,
    }
  );
  return (
    <>
      <Head>
        <title>Leaderboard | ChatJeopardy</title>
        <meta
          name="description"
          content="Choose your category and answer trivia questions!"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navigation />
      <main className="container mx-auto mt-4">
        <h1 className="mb-2 text-4xl font-bold">Leaderboard</h1>
        <div className="flex flex-col gap-4 rounded-lg bg-base-200 px-4 py-2 ">
          <table className="table-zebra table">
            <thead>
              <tr>
                <th>Rank</th>
                <th>User</th>
                <th
                  className="cursor-pointer"
                  onClick={() => {
                    if (sort !== "accuracy") {
                      setSort("accuracy");
                      setOrder("desc");
                    } else {
                      setOrder(order === "asc" ? "desc" : "asc");
                    }
                  }}
                >
                  Accuracy %
                  {sort === "accuracy" &&
                    (order === "desc" ? (
                      <ArrowDownIcon className="ml-2 inline-block h-4 w-4 text-lg" />
                    ) : (
                      <ArrowUpIcon className="ml-2 inline-block h-4 w-4 text-lg" />
                    ))}
                </th>
                <th
                  className="cursor-pointer"
                  onClick={() => {
                    if (sort !== "questionsCorrect") {
                      setSort("questionsCorrect");
                      setOrder("desc");
                    } else {
                      setOrder(order === "asc" ? "desc" : "asc");
                    }
                  }}
                >
                  Correct Answers
                  {sort === "questionsCorrect" &&
                    (order === "desc" ? (
                      <ArrowDownIcon className="ml-2 inline-block h-4 w-4 text-lg" />
                    ) : (
                      <ArrowUpIcon className="ml-2 inline-block h-4 w-4 text-lg" />
                    ))}
                </th>
                <th
                  className="cursor-pointer"
                  onClick={() => {
                    if (sort !== "questionsAnswered") {
                      setSort("questionsAnswered");
                      setOrder("desc");
                    } else {
                      setOrder(order === "asc" ? "desc" : "asc");
                    }
                  }}
                >
                  Questions Answered
                  {sort === "questionsAnswered" &&
                    (order === "desc" ? (
                      <ArrowDownIcon className="ml-2 inline-block h-4 w-4 text-lg" />
                    ) : (
                      <ArrowUpIcon className="ml-2 inline-block h-4 w-4 text-lg" />
                    ))}
                </th>
              </tr>
            </thead>
            {!!leaderboardData && (
              <PageContext.Provider
                value={{
                  page,
                  setPage,
                  numPages: Math.ceil(leaderboardData?.totalUsers / 10),
                }}
              >
                <tbody>
                  {leaderboardData?.leaderboard.map((user, index) => (
                    <tr key={index}>
                      <th>#{(page - 1) * 10 + index + 1}</th>
                      <td>
                        <Link
                          href={`/account?userId=${user.userId}`}
                          className="align-middle text-blue-500 hover:text-blue-600"
                        >
                          <div className="flex items-center gap-2">
                            <div className="mask mask-hexagon inline-block h-8 w-8">
                              <Image
                                src={user.image ?? ""}
                                width={40}
                                height={40}
                                alt={user.name ?? ""}
                                className="h-8 w-8"
                              />
                            </div>
                            {user.name}
                          </div>
                        </Link>
                      </td>
                      <td>{(user.accuracy * 100).toFixed(1)}%</td>
                      <td>{user.questionsCorrect.toString()}</td>
                      <td>{user.questionsAnswered.toString()}</td>
                    </tr>
                  ))}
                </tbody>
              </PageContext.Provider>
            )}
          </table>
          <Pagination />
        </div>
      </main>
    </>
  );
};

export default Leaderboard;
