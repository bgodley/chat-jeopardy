import { type GetServerSidePropsContext, type NextPage } from "next";
import Head from "next/head";
import { useReducer } from "react";
import { api } from "../utils/api";
import { getServerAuthSession } from "@/server/auth";
import { signIn, useSession } from "next-auth/react";
import Navigation from "@/components/Navigation";
import { ArrowPathIcon } from "@heroicons/react/24/solid";

interface PageState {
  stage: "waiting" | "question" | "response";
  category?: string;
  question?: {
    question: string;
    questionId: string;
    correctAnswer?: string;
    wasCorrect?: boolean;
  };
  userAnswer?: string;
  answerResponse?: string;
}

type PageAction =
  | { type: "category"; payload: { category: string } }
  | { type: "question"; payload: { question: string; questionId: string } }
  | { type: "userAnswer"; payload: { userAnswer: string } }
  | {
      type: "response";
      payload: { response: string; correctAnswer: string; correct: boolean };
    }
  | { type: "reset" };

const pageReducer = (state: PageState, action: PageAction): PageState => {
  switch (action.type) {
    case "category":
      return {
        ...state,
        stage: "waiting",
        category: action.payload.category,
        question: undefined,
        userAnswer: undefined,
        answerResponse: undefined,
      };
    case "question":
      return {
        ...state,
        stage: "question",
        question: {
          question: action.payload.question,
          questionId: action.payload.questionId,
        },
        userAnswer: undefined,
        answerResponse: undefined,
      };
    case "userAnswer":
      return {
        ...state,
        stage: "question",
        userAnswer: action.payload.userAnswer,
        answerResponse: undefined,
      };
    case "response":
      const { response, correctAnswer, correct } = action.payload;
      return {
        ...state,
        stage: "response",
        answerResponse: response,
        question: {
          question: state.question?.question as string,
          questionId: state.question?.questionId as string,
          correctAnswer: correctAnswer,
          wasCorrect: correct,
        },
      };
    case "reset":
      return {
        stage: "waiting",
      };
    default:
      return state;
  }
};

const Play: NextPage = () => {
  const { data: session } = useSession();
  const [state, dispatch] = useReducer(pageReducer, { stage: "waiting" });

  if (!session) {
    return (
      <div className="container mx-auto flex flex-col gap-[12px] px-2 pt-10">
        <h1 className="text-4xl font-bold">ChatJeopardy</h1>
        <p className="text-lg">You must be signed in to play.</p>
        <div>
          <button className="btn-primary btn" onClick={() => void signIn("auth0")}>
            Sign in
          </button>
        </div>
      </div>
    );
  }

  const {
    mutate: getQuestion,
    isLoading: loadingQuestion,
    isError: questionError,
  } = api.game.getQuestion.useMutation({
    onSuccess: ({ question, questionId }) => {
      dispatch({
        type: "question",
        payload: {
          question,
          questionId,
        },
      });
    },
    retry(failureCount, error) {
      if (error.data?.httpStatus === 400) {
        return false;
      }
      return failureCount < 3;
    },
  });

  const {
    mutate: submitAnswer,
    isLoading: loadingResponse,
    isError: answerError,
  } = api.game.submitAnswer.useMutation({
    onSuccess: ({ response, correct, correctAnswer }) => {
      dispatch({
        type: "response",
        payload: {
          response,
          correct,
          correctAnswer,
        },
      });
    },
    retry(failureCount, error) {
      if (error.data?.httpStatus === 400) {
        return false;
      }
      return failureCount < 3;
    },
  });

  const {
    data: myCategories,
    isLoading: loadingCategories,
    isError: errorCategories,
  } = api.game.getMyCategories.useQuery();
  const loading = loadingQuestion || loadingResponse;

  const {
    data: mySuggestions,
    isFetching: loadingSuggestions,
    isError: errorSuggestions,
    refetch: refetchSuggestions
  } = api.game.getCategorySuggestions.useQuery(
    {
      categories: myCategories?.categories,
    },
    {
      enabled: !!myCategories,
      staleTime: 1000 * 60 * 10,
    }
  );

  return (
    <>
      <Head>
        <title>Play | ChatJeopardy</title>
        <meta name="description" content="Choose your category and answer trivia questions!" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navigation />
      <main className="container mx-auto flex flex-col gap-[12px] px-2 mt-4">
        <h1 className="text-4xl font-bold">ChatJeopardy</h1>
        {state.stage === "waiting" && (
          <>
            <div className="flex flex-col gap-4 rounded-lg bg-base-200 p-4 ">
              <p className="text-lg">
                Choose a category and we&apos;ll generate a question for you.
              </p>
              <input
                type="text"
                placeholder="Category"
                disabled={loading}
                className="input-bordered input w-full"
                value={state.category}
                onChange={(e) =>
                  dispatch({
                    type: "category",
                    payload: {
                      category: e.target.value,
                    },
                  })
                }
                onKeyDown={(e) => {
                  if (e.key === "Enter") {
                    if (!state.category) return;
                    getQuestion({
                      category: state.category,
                    });
                  }
                }}
              />
              <div className="flex flex-row flex-wrap items-center gap-[12px]">
                <p>Previous categories:</p>
                {loadingCategories && (
                  <span className="loading loading-dots loading-md" />
                )}
                {errorCategories && <p className="text-red-500">Error</p>}
                {myCategories?.categories.length === 0 && <p>None</p>}
                {myCategories?.categories?.map((category) => (
                  <button
                    key={category}
                    className="btn-primary btn-sm btn"
                    disabled={loading}
                    onClick={() => {
                      dispatch({
                        type: "category",
                        payload: {
                          category,
                        },
                      });

                      getQuestion({
                        category,
                      });
                    }}
                  >
                    {category}
                  </button>
                ))}
              </div>
              <div className="flex flex-row flex-wrap items-center gap-[12px]">
                <p>Suggested categories:</p>
                {loadingSuggestions && (
                  <span className="loading loading-dots loading-md" />
                )}
                {errorSuggestions && <p className="text-red-500">Error</p>}
                {mySuggestions?.suggestions.length === 0 && <p>None</p>}
                {mySuggestions?.suggestions?.map((category) => (
                  <button
                    key={category}
                    className="btn-secondary btn-sm btn"
                    disabled={loading}
                    onClick={() => {
                      dispatch({
                        type: "category",
                        payload: {
                          category,
                        },
                      });

                      getQuestion({
                        category,
                      });
                    }}
                  >
                    {category}
                  </button>
                ))}
                {!!mySuggestions && <button
                  className="btn-info btn-sm btn px-1"
                  disabled={loadingSuggestions}
                  onClick={() => {
                    void refetchSuggestions();
                  }}
                >
                  <ArrowPathIcon className="w-4 h-4" />
                </button>}
              </div>
              <div className="flex flex-row flex-wrap items-center gap-[12px]">
                <button
                  className="btn-primary btn"
                  disabled={loading || !state.category}
                  onClick={() => {
                    if (!state.category) return;
                    getQuestion({
                      category: state.category,
                    });
                  }}
                >
                  Get question
                  {loadingQuestion && (
                    <span className="loading loading-dots loading-md" />
                  )}
                </button>
                {questionError && (
                  <p className="text-red-500">
                    An error occurred while generating the question. Please try
                    again.
                  </p>
                )}
              </div>
            </div>
          </>
        )}
        {state.stage === "question" && (
          <>
            <div className="flex flex-col gap-4 rounded-lg bg-base-200 p-4 ">
              <div>
                <p className="text-sm text-gray-400">Category</p>
                <h3 className="text-2xl font-bold">{state.category}</h3>
              </div>
              <p className="text-lg">{state.question?.question}</p>
              <input
                type="text"
                placeholder="Your answer"
                disabled={loading}
                className="input-bordered input w-full"
                value={state.userAnswer}
                onChange={(e) =>
                  dispatch({
                    type: "userAnswer",
                    payload: {
                      userAnswer: e.target.value,
                    },
                  })
                }
                onKeyDown={(e) => {
                  if (e.key === "Enter") {
                    if (!state.question || !state.userAnswer) return;
                    submitAnswer({
                      questionId: state.question?.questionId,
                      userAnswer: state.userAnswer,
                    });
                  }
                }}
              />
              <div className="flex flex-row flex-wrap items-center gap-[12px]">
                <button
                  className="btn-primary btn"
                  disabled={loading || !state.category}
                  onClick={() => {
                    if (!state.question || !state.userAnswer) return;
                    submitAnswer({
                      questionId: state.question?.questionId,
                      userAnswer: state.userAnswer,
                    });
                  }}
                >
                  Submit
                  {loadingResponse && (
                    <span className="loading loading-dots loading-md" />
                  )}
                </button>
                <button
                  className="btn-secondary btn"
                  disabled={loading || !state.category}
                  onClick={() => {
                    if (!state.category) return;
                    getQuestion({
                      category: state.category,
                    });
                  }}
                >
                  Skip Question
                  {loadingQuestion && (
                    <span className="loading loading-dots loading-md" />
                  )}
                </button>

                <button
                  className="btn-secondary btn"
                  disabled={loading || !state.category}
                  onClick={() => {
                    dispatch({
                      type: "reset",
                    });
                  }}
                >
                  Choose new category
                </button>

                {answerError && (
                  <p className="text-red-500">
                    An error occurred while checking your answer. Please try
                    again.
                  </p>
                )}
              </div>
            </div>
          </>
        )}
        {state.stage === "response" && (
          <>
            <div className="flex flex-col gap-4 rounded-lg bg-base-200 p-4">
              <div>
                <p className="text-sm text-gray-400">Category</p>
                <h3 className="text-2xl font-bold">{state.category}</h3>
              </div>
              <div>
                <p className="text-sm text-gray-400">Question</p>
                <p className="text-lg italic">&quot;{state.question?.question}&quot;</p>
              </div>
              <div>
                <p className="text-sm text-gray-400">You said...</p>
                <p className="text-lg italic">&quot;{state.userAnswer}&quot;</p>
              </div>
              <div className="flex flex-row gap-12">
                <div>
                  <p className="text-sm text-gray-400">You were...</p>
                  <p
                    className={
                      state.question?.wasCorrect
                        ? "text-2xl font-bold text-green-600 dark:text-green-500"
                        : "text-xl font-bold text-red-600 dark:text-red-500"
                    }
                  >
                    {state.question?.wasCorrect ? "Correct" : "Incorrect"}
                  </p>
                </div>
                <div>
                  <p className="text-sm text-gray-400">Correct answer</p>
                  <p className="text-lg">{state.question?.correctAnswer}</p>
                </div>
              </div>
              <div>
                <p className="text-sm text-gray-400">ChatJeopardy says...</p>
                <p className="text-lg">{state.answerResponse}</p>
              </div>
              <div className="flex flex-row flex-wrap items-center gap-[12px]">
                <button
                  className="btn-primary btn"
                  disabled={loading || !state.category}
                  onClick={() => {
                    if (!state.category) return;
                    getQuestion({
                      category: state.category,
                    });
                  }}
                >
                  Next Question
                  {loadingQuestion && (
                    <span className="loading loading-dots loading-md" />
                  )}
                </button>

                <button
                  className="btn-secondary btn"
                  disabled={loading || !state.category}
                  onClick={() => {
                    if (!state.question || !state.userAnswer) return;
                    submitAnswer({
                      reconsider: true,
                      questionId: state.question.questionId,
                      userAnswer: state.userAnswer,
                    });
                  }}
                >
                  Reconsider
                  {loadingResponse && (
                    <span className="loading loading-dots loading-md" />
                  )}
                </button>

                <button
                  className="btn-secondary btn"
                  disabled={loading || !state.category}
                  onClick={() => {
                    dispatch({
                      type: "reset",
                    });
                  }}
                >
                  Choose new category
                </button>
              </div>
            </div>
          </>
        )}
      </main>
    </>
  );
};

export default Play;

export async function getServerSideProps(context: GetServerSidePropsContext) {
  return {
    props: {
      session: await getServerAuthSession(context),
    },
  };
}
