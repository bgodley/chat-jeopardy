import { gameRouter } from "@/server/api/routers/game";
import { createTRPCRouter } from "@/server/api/trpc";
import { accountRouter } from "./routers/account";
import { leaderboardRouter } from "./routers/leaderboard";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  game: gameRouter,
  account: accountRouter,
  leaderboard: leaderboardRouter
});

// export type definition of API
export type AppRouter = typeof appRouter;
