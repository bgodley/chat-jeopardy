import { z } from "zod";
import { createTRPCRouter, publicProcedure } from "@/server/api/trpc";
import { prisma } from "@/server/db";
import { QuestionStatus } from "@prisma/client";

export const accountRouter = createTRPCRouter({

  getAccount: publicProcedure
    .input(
      z.object({
        userId: z.string().min(10).max(100).optional(),
      })
    )
    .query(async ({ ctx, input: { userId } }) => {
      userId = userId ?? ctx.session?.user.id;

      if (!userId) throw new Error("User not found");

      const user = await prisma.user.findUnique({
        where: {
          id: userId,
        },
        select: {
          id: true,
          name: true,
          image: true
        }
      });

      if (!user) throw new Error("User not found");

      return {
        user,
      };
    }),

  getHistory: publicProcedure
    .input(
      z.object({
        userId: z.string().min(10).max(100).optional(),
        page: z.number().int().min(0).max(1000),
      })
    )
    .query(async ({ ctx, input: { userId, page } }) => {
      userId = userId ?? ctx.session?.user.id;

      if (!userId) throw new Error("User not found");
      
      const questions = await prisma.question.findMany({
        where: {
          userId,
          OR: [
            { questionStatus: QuestionStatus.skipped },
            { questionStatus: QuestionStatus.answered },
          ],
        },
        orderBy: {
          createdAt: "desc",
        },
        take: 10,
        skip: 10 * (page - 1),
      });

      const total = await prisma.question.count({
        where: {
          userId,
          OR: [
            { questionStatus: QuestionStatus.skipped },
            { questionStatus: QuestionStatus.answered },
          ],
        },
      });

      return {
        questions,
        total,
      };
    }),

  getStats: publicProcedure
    .input(
      z.object({
        userId: z.string().min(10).max(100).optional(),
      })
    )
    .query(async ({ ctx, input: { userId } }) => {
      userId = userId ?? ctx.session?.user.id;

      if (!userId) throw new Error("User not found");

      const correctAnswers = await prisma.question.count({
        where: {
          userId,
          wasCorrect: true,
        },
      });

      const skippedQuestions = await prisma.question.count({
        where: {
          userId,
          questionStatus: QuestionStatus.skipped,
        },
      });

      const answeredQuestions = await prisma.question.count({
        where: {
          userId,
          questionStatus: QuestionStatus.answered,
        },
      });

      const topCategories = await prisma.question
        .groupBy({
          take: 5,
          by: ["category"],
          _count: {
            category: true,
          },
          where: {
            userId,
            OR: [
              { questionStatus: QuestionStatus.skipped },
              { questionStatus: QuestionStatus.answered },
            ],
          },
          orderBy: [
            {
              _count: {
                category: "desc",
              },
            },
          ],
        })
        .then((categories) => {
          return categories.map((category) => ({
            category: category.category,
            count: category._count.category,
          }));
        });

      return {
        topCategories,
        correctAnswers,
        skippedQuestions,
        answeredQuestions,
      };
    }),
});
