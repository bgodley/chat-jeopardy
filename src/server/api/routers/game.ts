import { z } from "zod";
import { createTRPCRouter, protectedProcedure } from "@/server/api/trpc";
import { chat } from "@/utils/chat";
import { prisma } from "@/server/db";
import { QuestionStatus } from "@prisma/client";

export const gameRouter = createTRPCRouter({
  getQuestion: protectedProcedure
    .input(
      z.object({
        category: z.string().min(1).max(400),
      })
    )
    .mutation(async ({ ctx, input: { category } }) => {
      const userId = ctx.session.user.id;
      console.log("category", category);

      const prompt2 = `Please generate a trivia question and its answer for the category you generated. 
        The trivia question should be as original, specific, and factual as possible.
        The answer must be a single word or phrase.
        Include useful hints and context in the question, like time period, location, field of knowledge, etc.

        Respond with the question and answer in the following format:
        
        Question: What is the capital of the United States?
        Answer: Washington, D.C.`;

      const { response } = await chat([
        {
          role: "system",
          content: "Welcome to Jeopardy! I am your host, Alex Trebek.",
        },
        {
          role: "system",
          content:
            "Please generate a category for Jeopardy. The category should be as original as possible, while still remaining in the realm of factual trivia.",
        },
        { role: "assistant", content: category },
        { role: "system", content: prompt2 },
      ]);

      const lines = response.content.split("\n");
      const question = lines
        .find((line) => line.startsWith("Question: "))
        ?.replace("Question: ", "");
      const correctAnswer = lines
        .find((line) => line.startsWith("Answer: "))
        ?.replace("Answer: ", "");

      console.error(response.content);
      if (!question || !correctAnswer)
        throw new Error("Invalid response from OpenAI");

      // Check if user already has active question, set it as skipped if so.
      await prisma.question.updateMany({
        where: {
          userId,
          questionStatus: QuestionStatus.active,
        },
        data: {
          questionStatus: QuestionStatus.skipped,
        },
      });

      const { id: questionId } = await prisma.question.create({
        data: {
          question,
          correctAnswer,
          userId,
          category,
        },
      });

      return {
        question,
        questionId,
      };
    }),

  submitAnswer: protectedProcedure
    .input(
      z.object({
        questionId: z.string().min(1).max(64),
        reconsider: z.boolean().optional(),
        userAnswer: z.string().min(1).max(400),
      })
    )
    .mutation(async ({ input: { questionId, reconsider, userAnswer } }) => {
      const question = await prisma.question.findFirst({
        where: {
          id: questionId,
        },
      });

      if (!question) throw new Error("Question not found");
      if (reconsider && question.questionStatus !== QuestionStatus.answered)
        throw new Error("Question not answered yet");
      if (reconsider && question.reconsidered)
        throw new Error("Question already reconsidered");
      if (!reconsider && question.questionStatus !== QuestionStatus.active)
        throw new Error("Question not active");

      if (!reconsider) {
        const { response } = await chat([
          {
            role: "system",
            content: `The following question and correct answer have been generated for you.
            Question: ${question.question}
  
            Correct Answer: ${question.correctAnswer}`,
          },
          {
            role: "system",
            content: `Please determine whether the contestant was correct, step-by-step.
            Use the following format:
            
            Correct. or Incorrect.
            <explanation>`,
          },
          {
            role: "user",
            content: "My answer is: " + userAnswer,
            name: "Contestant",
          },
        ]);

        const correct = response.content.split("\n")[0]?.includes("Correct");

        if (correct === undefined)
          throw new Error("Invalid response from OpenAI");

        await prisma.question.update({
          where: {
            id: questionId,
          },
          data: {
            userAnswer,
            wasCorrect: correct,
            questionStatus: QuestionStatus.answered,
          },
        });

        return {
          correct,
          correctAnswer: question.correctAnswer,
          response: response.content,
        };
      } else {
        // Reconsider
        const { response } = await chat([
          {
            role: "system",
            content: `The following question was asked of the Contestant.
            Question: ${question.question}`,
          },
          {
            role: "user",
            content: "My answer is: " + userAnswer,
            name: "Contestant",
          },
          {
            role: "system",
            content: `Please determine whether the contestant was correct, step-by-step.
            Use the following format:
            
            Correct. or Incorrect.
            <explanation>`,
          },
        ]);

        const correct = response.content.split("\n")[0]?.includes("Correct");

        if (correct === undefined)
          throw new Error("Invalid response from OpenAI");

        await prisma.question.update({
          where: {
            id: questionId,
          },
          data: {
            wasCorrect: correct,
            userAnswer,
            questionStatus: QuestionStatus.answered,
            reconsidered: true,
          },
        });

        return {
          correctAnswer: question.correctAnswer,
          correct,
          response: response.content,
        };
      }
    }),

  getMyCategories: protectedProcedure.query(async ({ ctx }) => {
    const userId = ctx.session.user.id;

    const categories = await prisma.question
      .findMany({
        where: {
          userId,
        },
        select: {
          category: true,
        },
        distinct: ["category"],
        orderBy: {
          createdAt: "desc",
        },
        take: 3,
      })
      .then((categories) => {
        return categories.map((category) => category.category);
      });

    return {
      categories,
    };
  }),

  getCategorySuggestions: protectedProcedure
    .input(
      z.object({
        categories: z.array(z.string().min(1).max(64)).optional(),
      })
    )
    .query(async ({ input: { categories } }) => {
      if (categories === undefined) {
        const { response: lines } = await chat([
          {
            role: "system",
            content: `Please generate three categories for Jeopardy.
              The categories should be as original as possible, while still remaining in the realm of factual trivia.
              Format your answer with only one category per line. Do not respond with anything except the suggested categories.`,
          },
        ]);
        const suggestions = lines.content
          .split("\n")
          .map((line) => line.replace("^s*d+.s*", ""))
          .map((line) => line.trim())
          .filter((line) => line.length > 0 && line.length < 64);

        return {
          suggestions,
        };
      } else {
        const { response: lines } = await chat([
          {
            role: "system",
            content: `Please generate three categories for Jeopardy.
              The categories should be as original as possible, while still remaining in the realm of factual trivia.
              The suggested categories should be related to the following categories: 
              ${categories.join(", ")}.
              Format your answer with only one category per line. Do not respond with anything except the suggested categories.`,
          },
        ]);

        const suggestions = lines.content
          .split("\n")
          .map((line) => line.replace("^s*d+.s*", ""))
          .map((line) => line.trim())
          .filter((line) => line.length > 0 && line.length < 64);

        return {
          suggestions,
        };
      }
    }),
});
