import { z } from "zod";
import { createTRPCRouter, publicProcedure } from "@/server/api/trpc";
import { type User } from "@prisma/client";

export const leaderboardRouter = createTRPCRouter({
  getLeaderboard: publicProcedure
    .input(
      z.object({
        page: z.number().int().min(0).max(1000),
        sort: z.enum(["questionsAnswered", "questionsCorrect", "accuracy"]).optional(),
        order: z.enum(["asc", "desc"]).optional(),
      })
    )
    .query(async ({ input: { page, sort, order }, ctx }) => {
      type LeaderboardUser = {
        userId: User["id"];
        name: User["name"];
        image: User["image"];
        questionsAnswered: number;
        questionsCorrect: number;
        accuracy: number;
      };

      sort = sort ?? "questionsAnswered";
      order = order ?? "desc";

      const totalUsers = await ctx.prisma.user.count();

      const sql = `
        SELECT 
          User.id AS "userId",
          User.name AS "name",
          User.image AS "image",
          IFNULL(AnsweredQuestions.total, 0) AS "questionsAnswered",
          IFNULL(CorrectQuestions.total, 0) AS "questionsCorrect",
          IFNULL(CorrectQuestions.total, 0) / IFNULL(AnsweredQuestions.total, 1) AS "accuracy"
        FROM
          User
        LEFT JOIN (
          SELECT
            Question.userId as "userId",
            COUNT(Question.id) as "total"
          FROM
            Question
          WHERE
            Question.questionStatus = 'answered'
          GROUP BY
            Question.userId
        ) AS AnsweredQuestions ON AnsweredQuestions.userId = User.id
        LEFT JOIN (
          SELECT
            Question.userId as "userId",
            COUNT(Question.id) as "total"
          FROM
            Question
          WHERE
            Question.questionStatus = 'answered' AND Question.wasCorrect = 1
          GROUP BY
            Question.userId
        ) AS CorrectQuestions ON CorrectQuestions.userId = User.id
        ORDER BY
          ${sort} ${order.toUpperCase()}
        LIMIT 10 OFFSET ${(page - 1) * 10}
      `;
      
      const leaderboard = await ctx.prisma.$queryRawUnsafe<LeaderboardUser[]>(sql);

      return {
        totalUsers,
        leaderboard,
      };
    }),
});
