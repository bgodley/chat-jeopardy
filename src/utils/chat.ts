import { type ChatCompletionRequestMessage, type ChatCompletionResponseMessage, Configuration, OpenAIApi } from "openai";

const OPENAI_API_KEY: string = process.env.OPENAI_API_KEY ?? "";

const configuration = new Configuration({
  apiKey: OPENAI_API_KEY
});

const openai = new OpenAIApi(configuration);

export const chat = async (messages: ChatCompletionRequestMessage[]): Promise<{ response: ChatCompletionResponseMessage, messages: ChatCompletionRequestMessage[]}> => {
  let response;
  let tries = 0;
  while (!response && tries < 3) {
    try {
      response = await openai.createChatCompletion({
        model: "gpt-3.5-turbo",
        messages: messages,
        max_tokens: 400,
        temperature: 1,
      });
    } catch (e) {
      console.error("Retrying...");
      tries++;
      await new Promise((resolve) => setTimeout(resolve, 1000));
    }
  }

  if (!response) throw new Error("Something went wrong");

  const choice = response.data.choices[0];
  if (choice === undefined || choice.message === undefined) throw new Error("No response from OpenAI");

  return {
    response: choice.message,
    messages: [
      ...messages,
      {
        role: "assistant",
        content: choice.message.content,
      },
    ],
  };
};
