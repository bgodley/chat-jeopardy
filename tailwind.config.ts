import { type Config } from "tailwindcss";

export default {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Avenir", "Montserrat", "Corbel", "'URW Gothic'", "source-sans-pro", "sans-serif"]
        // sans: ["Didot", "'Bodoni MT'", "'Noto Serif Display'", "'URW Palladio L'", "P052", "Sylfaen", "serif"]
        // sans: ["ui-rounded", "Quicksand", "Comfortaa", "Manjari", "'Arial Rounded MT'", "'Arial Rounded MT Bold'", "Calibri", "source-sans-pro", "sans-serif"]
        // sans: ["ui-rounded", "'Hiragino Maru Gothic ProN'", "Quicksand", "Comfortaa", "Manjari", "'Arial Rounded MT'", "'Arial Rounded MT Bold'", "Calibri", "source-sans-pro", "sans-serif"],
      }
    },
  },
  daisyui: {
    themes: [
      "winter",
      "night"
    ]
  },
  plugins: [require("daisyui")],
} satisfies Config;
